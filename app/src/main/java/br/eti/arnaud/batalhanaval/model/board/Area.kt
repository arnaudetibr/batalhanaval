package br.eti.arnaud.batalhanaval.model.board

import br.eti.arnaud.batalhanaval.model.ship.Piece

class Area (val piece: Piece, private val part: Int, val vertical: Boolean){

    var hit = false

    fun getPartRes(): Int {
        return piece.getPartRes(part, hit)
    }

}