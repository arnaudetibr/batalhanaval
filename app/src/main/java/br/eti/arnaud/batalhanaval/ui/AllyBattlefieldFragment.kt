package br.eti.arnaud.batalhanaval.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.ImageView
import br.eti.arnaud.batalhanaval.model.board.Area

class AllyBattlefieldFragment: BattlefieldFragment() {

    companion object { fun newInstance() = AllyBattlefieldFragment() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupObservers()

    }

    override fun onAreaClick(x: Int, y: Int) {

        vm.allyBattlefield.shoot(x,y)

    }

    override fun onUpdateArea(areaImageView: ImageView, area: Area) {
        // Nada especial para fazer aqui
    }

    private fun setupObservers() {

        vm.allyBattlefield.observable.board.observe(this, Observer { it -> it?.let { battlefield ->
            updateBoardUi(battlefield)
        }})

        vm.allyBattlefield.observable.shoot.observe(this, Observer {it -> it?.let { point ->
            blinkAt(point)
        }})

        vm.allyBattlefield.observable.vulnerable.observe(this, Observer { it -> it?.let { b ->
            isVulnerable = b
        }})

    }
}