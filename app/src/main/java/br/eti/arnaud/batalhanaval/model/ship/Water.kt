package br.eti.arnaud.batalhanaval.model.ship

import br.eti.arnaud.batalhanaval.R

class Water: Piece() {

    init {
        name = R.string.water
        pieces = arrayOf(0)
        destroyedPieces = arrayOf(R.drawable.splash)
    }


}