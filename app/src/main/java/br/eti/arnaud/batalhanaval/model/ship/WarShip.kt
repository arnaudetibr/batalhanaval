package br.eti.arnaud.batalhanaval.model.ship

import br.eti.arnaud.batalhanaval.R

class WarShip: Piece() {
    init {
        name = R.string.warship
        pieces = arrayOf(R.drawable.ship120, R.drawable.ship220)
        destroyedPieces = arrayOf(R.drawable.ship121, R.drawable.ship221)
    }
}