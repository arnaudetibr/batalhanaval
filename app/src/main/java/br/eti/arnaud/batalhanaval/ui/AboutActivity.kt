package br.eti.arnaud.batalhanaval.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import br.eti.arnaud.batalhanaval.R
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : BaseActivity() {

    companion object {

        fun start(context: Context){
            context.startActivity(Intent(context, AboutActivity::class.java))
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home){
            finish()
        }

        return super.onOptionsItemSelected(item)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        victoriesTextView.text = sharedPreferences.getInt(VICTORIES_SP, 0).toString()
        defeatsTextView.text = sharedPreferences.getInt(DEFEATS_SP, 0).toString()
    }
}
