package br.eti.arnaud.batalhanaval.model.board

import android.arch.lifecycle.MutableLiveData
import android.graphics.Point
import br.eti.arnaud.batalhanaval.R
import br.eti.arnaud.batalhanaval.model.ship.Boat
import br.eti.arnaud.batalhanaval.model.ship.Cargo
import br.eti.arnaud.batalhanaval.model.ship.WarShip
import br.eti.arnaud.batalhanaval.model.ship.Water

abstract class Battlefield {

    val observable = Observable()
    private var board: Array<Array<Area>>

    init {
        board = getCleanBoard()
    }

    abstract fun onDefeat()

    private fun getWaterArea(): Area{
        return Area(Water(), 1, false)
    }

    private fun getCleanBoard(): Array<Array<Area>> {
        return arrayOf(
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea()),
            arrayOf(getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea(), getWaterArea())
        )
    }

    fun distributeShips() {

        addCargoInRandomPosition()
        for (i in 1..2) addWarShipInRandomPosition()
        for (i in 1..3) addBoatInRandomPosition()
        observable.board.value = board

    }

    private fun addBoatInRandomPosition() {
        val x = Bot.rd(0, 6)
        val y = Bot.rd (0, 4)
        if (board[x][y].piece is Water){
            val boat = Boat()
            board[x][y] = Area(boat, 1, false)
        } else {
            addBoatInRandomPosition()
        }
    }

    private fun addWarShipInRandomPosition() {
        val z = Bot.rd(0, 1)
        val isVertical = z == 1
        val xLimit = if (isVertical) 6 else 5
        val yLimit = if (isVertical) 3 else 4
        val x = Bot.rd(0, xLimit)
        val y = Bot.rd(0, yLimit)

        if (board[x][y].piece !is Water
            || (isVertical && board[x][y+1].piece !is Water)
            || (!isVertical && board[x+1][y].piece !is Water)){
            addWarShipInRandomPosition()
        } else {
            val warShip = WarShip()
            board[x][y] = Area(warShip, 1, isVertical)
            if (isVertical){
                board[x][y+1] = Area(warShip, 2, isVertical)
            } else {
                board[x+1][y] = Area(warShip, 2, isVertical)
            }
        }
    }

    private fun addCargoInRandomPosition() {
        val z = Bot.rd(0, 1)
        val isVertical = z == 1
        val xLimit = if (isVertical) 6 else 4
        val yLimit = if (isVertical) 2 else 4
        val x = Bot.rd(0, xLimit)
        val y = Bot.rd (0, yLimit)

        if (board[x][y].piece !is Water
            || (isVertical && board[x][y+1].piece !is Water)
            || (isVertical && board[x][y+2].piece !is Water)
            || (!isVertical && board[x+1][y].piece !is Water)
            || (!isVertical && board[x+2][y].piece !is Water)){
            addCargoInRandomPosition()
        } else {
            val cargo = Cargo()
            board[x][y] = Area(cargo, 1, isVertical)
            if (isVertical){
                board[x][y+1] = Area(cargo, 2, isVertical)
                board[x][y+2] = Area(cargo, 3, isVertical)
            } else {
                board[x+1][y] = Area(cargo, 2, isVertical)
                board[x+2][y] = Area(cargo, 3, isVertical)
            }
        }
    }

    fun clearBoard() {
        board = getCleanBoard()
        observable.board.value = board
    }

    fun shoot(x: Int, y: Int): Boolean {
        val boardArea= board[x][y]
        if (!boardArea.hit){
            boardArea.hit = true
            observable.board.value = board
            observable.sound.value = if (boardArea.piece is Water) R.raw.miss else R.raw.explosion
            observable.shoot.value = Point(x,y)
            if (isDefeated()){
                observable.defeated.value = true
                onDefeat()
            } else {
                observable.vulnerable.value = boardArea.piece !is Water
            }

            return true
        }
        return false
    }

    private fun isDefeated(): Boolean {
        board.mapIndexed { x, column ->
            column.mapIndexed { y, area ->
                if (area.piece !is Water && !area.hit){
                    return false
                }
            }
        }
        return true
    }

    class Observable {
        val board = MutableLiveData<Array<Array<Area>>>()
        val sound = MutableLiveData<Int>()
        val shoot = MutableLiveData<Point>()
        val vulnerable = MutableLiveData<Boolean>()
        val defeated = MutableLiveData<Boolean>()
    }
}