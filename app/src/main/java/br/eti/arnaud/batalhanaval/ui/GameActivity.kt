package br.eti.arnaud.batalhanaval.ui

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.media.MediaPlayer
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import br.eti.arnaud.batalhanaval.R


class GameActivity : BaseActivity() {

    var bgm: MediaPlayer? = null
    private var soundEffect: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_activity)

        if (savedInstanceState == null){
            instanceBattleFieldsFragment()
            vm.restartGame()
        }

        setupObservables()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.game_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.action_sound -> {
                val sound = !sharedPreferences.getBoolean(SOUND_SP, true)
                sharedPreferences.edit().putBoolean(SOUND_SP, sound).apply()
                if (sound){
                    item.setIcon(R.drawable.ic_sound_on)
                    startBgm()
                } else {
                    item.setIcon(R.drawable.ic_sound_off)
                    stopBgm()
                }
                return true
            }
            R.id.action_about -> {
                AboutActivity.start(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun onStart() {
        super.onStart()

        startBgm()
    }

    override fun onStop() {
        super.onStop()

        stopBgm()
    }

    private fun startBgm() {
        if (!sharedPreferences.getBoolean(SOUND_SP, true)) return
        stopBgm()
        bgm = MediaPlayer.create(this, R.raw.bgm)
        bgm?.isLooping = true
        bgm?.start()
    }

    private fun stopBgm(){
        if (bgm != null && bgm!!.isPlaying) bgm?.stop()
        bgm?.release()
        bgm = null
    }

    private fun setupObservables() {
        vm.allyBattlefield.observable.vulnerable.observe(this, Observer { it ->
            it?.let {
                if (!it) {
                    vm.enemyBattlefield.observable.vulnerable.value = true
                } else {
                    vm.bot.start()
                }
            }
        })

        vm.enemyBattlefield.observable.vulnerable.observe(this, Observer { it ->
            it?.let {
                if (!it) {
                    vm.allyBattlefield.observable.vulnerable.value = true
                }
            }
        })

        vm.allyBattlefield.observable.sound.observe(this, Observer { it ->
            it?.let { soundRes ->
                playSound(soundRes)
            }
        })

        vm.enemyBattlefield.observable.sound.observe(this, Observer { it ->
            it?.let { soundRes ->
                playSound(soundRes)
            }
        })

        vm.enemyBattlefield.observable.defeated.observe(this, Observer {
            if (it == true) {
                incrementVictory()
                showGameOverDialog(getString(R.string.victory))
            }
        })

        vm.allyBattlefield.observable.defeated.observe(this, Observer {
            if (it == true) {
                incrementDefeat()
                showGameOverDialog(getString(R.string.defeat))
            }
        })
    }

    private fun incrementVictory() {
        val victories = sharedPreferences.getInt(VICTORIES_SP, 0)
        sharedPreferences.edit().putInt(VICTORIES_SP, victories + 1).apply()
    }

    private fun incrementDefeat() {
        val defeats = sharedPreferences.getInt(DEFEATS_SP, 0)
        sharedPreferences.edit().putInt(DEFEATS_SP, defeats + 1).apply()
    }

    private fun showGameOverDialog(title: String) {
        val adb = AlertDialog.Builder(this)
        adb.setTitle(title)
        adb.setMessage(getString(R.string.play_again))
        adb.setCancelable(false)
        adb.setPositiveButton(getString(R.string.yes)) { _, _ ->
            vm.restartGame()
        }
        adb.setNegativeButton(getString(R.string.exit)) { _, _ ->
            finish()
        }
        adb.create().show()
    }

    private fun instanceBattleFieldsFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.enemyBoardCointainer, EnemyBattlefieldFragment.newInstance())
            .replace(R.id.allyBoardCointainer, AllyBattlefieldFragment.newInstance())
            .commitNow()
    }

    private fun playSound(soundRes: Int) {
        if (!sharedPreferences.getBoolean(SOUND_SP, true)) return
        if(soundEffect != null) {
            if (soundEffect!!.isPlaying) soundEffect?.stop()
            soundEffect!!.release()
            soundEffect = null
        }
        soundEffect = MediaPlayer.create(this, soundRes)
        soundEffect?.start()
    }

}
