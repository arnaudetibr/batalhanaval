package br.eti.arnaud.batalhanaval.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.ImageView
import br.eti.arnaud.batalhanaval.model.board.Area

class EnemyBattlefieldFragment: BattlefieldFragment() {

    companion object { fun newInstance() = EnemyBattlefieldFragment() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupObservers()
        setupListeners()

        isVulnerable = true
    }

    private fun setupListeners() {
        boardMap.mapIndexed { x, column ->
            column.mapIndexed { y, areaImageView ->
                areaImageView.setOnClickListener{
                    if (isVulnerable){
                        onAreaClick(x,y)
                    }
                }
            }
        }
    }

    override fun onAreaClick(x: Int, y: Int) {

        vm.enemyBattlefield.shoot(x,y)

    }

    override fun onUpdateArea(areaImageView: ImageView, area: Area) {

        if (!area.hit){
            areaImageView.setImageResource(0)
        }

    }

    private fun setupObservers() {

        vm.enemyBattlefield.observable.board.observe(this, Observer { it -> it?.let { battlefield ->
            updateBoardUi(battlefield)
        }})

        vm.enemyBattlefield.observable.shoot.observe(this, Observer {it -> it?.let { point ->
            blinkAt(point)
        }})

        vm.enemyBattlefield.observable.vulnerable.observe(this, Observer { it -> it?.let { b ->
            isVulnerable = b
        }})

    }
}