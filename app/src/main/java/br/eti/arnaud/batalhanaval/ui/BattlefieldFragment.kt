package br.eti.arnaud.batalhanaval.ui

import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import br.eti.arnaud.batalhanaval.R
import br.eti.arnaud.batalhanaval.model.board.Area
import kotlinx.android.synthetic.main.battlefield_fragment.*


abstract class BattlefieldFragment : BaseFragment() {

    lateinit var boardMap: Array<Array<ImageView>>
    var isVulnerable = false

    protected abstract fun onAreaClick(x: Int, y: Int)

    protected abstract fun onUpdateArea(areaImageView: ImageView, area: Area)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.battlefield_fragment, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupBoard()

    }

    private fun setupBoard() {

        boardMap = arrayOf(
            arrayOf(enemyArea00, enemyArea01, enemyArea02, enemyArea03, enemyArea04),
            arrayOf(enemyArea10, enemyArea11, enemyArea12, enemyArea13, enemyArea14),
            arrayOf(enemyArea20, enemyArea21, enemyArea22, enemyArea23, enemyArea24),
            arrayOf(enemyArea30, enemyArea31, enemyArea32, enemyArea33, enemyArea34),
            arrayOf(enemyArea40, enemyArea41, enemyArea42, enemyArea43, enemyArea44),
            arrayOf(enemyArea50, enemyArea51, enemyArea52, enemyArea53, enemyArea54),
            arrayOf(enemyArea60, enemyArea61, enemyArea62, enemyArea63, enemyArea64)
        )

    }

    protected fun updateBoardUi(battlefield: Array<Array<Area>>?) {

        battlefield?.mapIndexed { x, column ->
            column.mapIndexed { y, area ->
                val areaImageView = boardMap[x][y]
                areaImageView.setImageResource(area.getPartRes())
                areaImageView.rotation = if (area.vertical == true) 90f else 0f
                onUpdateArea(areaImageView, area)
            }
        }

    }

    protected fun blinkAt(point: Point){
        val imageView = boardMap[point.x][point.y]
        val animation = AlphaAnimation(1f, 0.5f)
        animation.duration = 100
        animation.interpolator = LinearInterpolator()
        animation.repeatCount = Animation.INFINITE
        animation.repeatMode = Animation.REVERSE
        imageView.startAnimation(animation)
        Handler().postDelayed({
            animation.cancel()
            imageView.alpha = 1f
        }, 1000)
    }

}
