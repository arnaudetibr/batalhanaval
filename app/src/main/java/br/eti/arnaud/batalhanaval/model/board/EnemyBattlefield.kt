package br.eti.arnaud.batalhanaval.model.board

import br.eti.arnaud.batalhanaval.R

class EnemyBattlefield: Battlefield() {

    override fun onDefeat() {

        observable.sound.value = R.raw.victory

    }

}