package br.eti.arnaud.batalhanaval.model.ship

import br.eti.arnaud.batalhanaval.R

class Cargo: Piece() {
    init {
        name = R.string.cargo
        pieces = arrayOf(R.drawable.ship130, R.drawable.ship230, R.drawable.ship330)
        destroyedPieces = arrayOf(R.drawable.ship131, R.drawable.ship231, R.drawable.ship331)
    }

}