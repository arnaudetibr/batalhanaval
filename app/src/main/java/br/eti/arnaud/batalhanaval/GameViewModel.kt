package br.eti.arnaud.batalhanaval

import android.arch.lifecycle.ViewModel
import br.eti.arnaud.batalhanaval.model.board.AllyBattlefield
import br.eti.arnaud.batalhanaval.model.board.Bot
import br.eti.arnaud.batalhanaval.model.board.EnemyBattlefield

class GameViewModel : ViewModel() {

    val enemyBattlefield = EnemyBattlefield()
    val allyBattlefield = AllyBattlefield()
    val bot = Bot(this)

    fun restartGame() {
        enemyBattlefield.clearBoard()
        enemyBattlefield.distributeShips()

        allyBattlefield.clearBoard()
        allyBattlefield.distributeShips()

        enemyBattlefield.observable.vulnerable.value = true
    }
}
