package br.eti.arnaud.batalhanaval.model.ship

import br.eti.arnaud.batalhanaval.R

class Boat: Piece() {

    init {
        name = R.string.boat
        pieces = arrayOf(R.drawable.ship110)
        destroyedPieces = arrayOf(R.drawable.ship111)
    }


}