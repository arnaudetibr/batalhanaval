package br.eti.arnaud.batalhanaval.ui

import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import br.eti.arnaud.batalhanaval.GameViewModel

abstract class BaseActivity: AppCompatActivity() {

    protected lateinit var vm: GameViewModel
    protected lateinit var sharedPreferences: SharedPreferences

    companion object {

        const val VICTORIES_SP = "VICTORIES_SP"
        const val DEFEATS_SP = "DEFEATS_SP"
        const val SOUND_SP = "SOUND_SP"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        vm = ViewModelProviders.of(this).get(GameViewModel::class.java)
    }
}