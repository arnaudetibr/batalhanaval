package br.eti.arnaud.batalhanaval.model.board

import android.os.Handler
import br.eti.arnaud.batalhanaval.GameViewModel
import java.util.*

class Bot(private val vm: GameViewModel) {

    companion object {

        fun rd(min: Int, max: Int): Int {
            val r = Random()
            return r.nextInt(max - min + 1) + min
        }

    }

    fun start() {
        Handler().postDelayed({
            shootAtRandomPosition()
        }, 1000)
    }

    private fun shootAtRandomPosition() {
        val x = rd(0,6)
        val y = rd(0,4)
        if (!vm.allyBattlefield.shoot(x,y)){
            shootAtRandomPosition()
        }
    }

}