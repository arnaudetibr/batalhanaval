package br.eti.arnaud.batalhanaval.model.ship

abstract class Piece {

    var name: Int = 0
    var pieces = arrayOf<Int>()
    var destroyedPieces = arrayOf<Int>()

    fun getPartRes(part: Int, destroyed: Boolean): Int {
        val p = if (destroyed) destroyedPieces else pieces
        return if (part <= p.size) p[part-1] else 0
    }


}