package br.eti.arnaud.batalhanaval.ui

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import br.eti.arnaud.batalhanaval.GameViewModel

abstract class BaseFragment: Fragment() {

    protected lateinit var vm: GameViewModel

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is FragmentActivity){
            vm = ViewModelProviders.of(context)[GameViewModel::class.java]
        }

    }
}